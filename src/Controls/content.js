import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ControlButtons from './components/ControlButtons';
import './content.css';

class Content extends Component {
  onCloseHandler = () => {
    console.log('close');
  }
  render() {
    return (
      <ControlButtons closeAction={this.onCloseHandler}/>
    );
  }
}

export default Content;

const content = document.createElement('div');
document.body.appendChild(content);
ReactDOM.render(<Content />, content);